using System;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Collections.Generic;
using System.Linq;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public sealed class MyStrategy : IStrategy {

        public MyCommand Commands;
        public Dictionary<long, MyVehicle> Vehicles = new Dictionary<long, MyVehicle>(1000);
        public List<MyVehicle> MyVehicles = new List<MyVehicle>(500);
        public List<MyVehicle> EnemyVehicles = new List<MyVehicle>(500);
        public static readonly MyStrategy ms;

        public MyButerBuilder ButerBuilder { get; private set; }
        public World World { get; private set; }
        public Game Game { get; private set; }
        public Player Me { get; private set; }
        public WeatherType[][] WeatherByCellXY { get; private set; }
        internal MyMainActions MyMainActions { get; private set; }
        public bool ButerIsComplet { get; private set; }

        public void Move(Player me, World world, Game game, Move move) {
            //Update
            UpdateUnits(me, world, game, move);

            //Action
            if (ButerBuilder != null)
            {
                if (Commands == null) Commands = ButerBuilder.Build();
                if (ButerBuilder.IsComplet)
                {
                    ButerBuilder = null;
                    MyMainActions = new MyMainActions(this);
                }
            }
            else
            {
                AddCommand(MyMainActions.Run());
            }

            //call commands
            if (Commands != null)
            {
                if (me.RemainingActionCooldownTicks == 0)
                {
                    if (Commands.Run(move))
                    {
                        Commands = null;
                    }
                }
                return;
            }
        }

        private void AddCommand(MyCommand myCommand)
        {
            if (myCommand != null) Commands = myCommand;
        }

        private void Init(World world)
        {
            //Init
            if (world.TickIndex == 0)
            {
                ButerBuilder = new MyButerBuilder(this);
                MyConsts.SqrTacticalNuclearStrikeRadius = Game.TacticalNuclearStrikeRadius * Game.TacticalNuclearStrikeRadius;
            }
        }

        private void UpdateUnits(Player me, World world, Game game, Move move)
        {
            World = world;
            Game = game;
            Me = me;

            WeatherByCellXY = World.WeatherByCellXY;

            foreach (var item in world.NewVehicles.Select(x => new MyVehicle(x)))
            {
                Vehicles[item.Id] = item;
                if (item.IsMy)
                {
                    MyVehicles.Add(item);
                }
                else
                {
                    EnemyVehicles.Add(item);
                }
            }

            foreach (var item in world.VehicleUpdates)
            {
                Vehicles[item.Id].Update(item, World.TickIndex);
                if (item.Durability == 0)
                {
                    var vehicle =  Vehicles[item.Id];
                    Vehicles.Remove(item.Id);
                    if (vehicle.IsMy)
                    {
                        MyVehicles.Remove(vehicle);
                    }
                    else
                    {
                        EnemyVehicles.Remove(vehicle);
                    }
                }
            }

            Init(world);
        }

        public MyStrategy()
        {
            MyCommand.ms = this;
        }
    }
}