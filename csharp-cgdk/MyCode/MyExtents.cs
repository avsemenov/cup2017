﻿using System;
using System.Collections.Generic;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public static class MyExtents
    {
        public static bool Equal(this double source, double next)
        {
            return Math.Abs(next - source) < 0.00001;
        }

        public static bool Equal(this double source, double next, double epsilon)
        {
            return Math.Abs(next - source) < epsilon;
        }

        public static MyUnit GetCenter(this IEnumerable<MyUnit> source)
        {
            var max = new MyUnit();
            var min = new MyUnit(double.MaxValue, double.MaxValue);
            foreach (var item in source)
            {
                if (item.X < min.X) min.X = item.X;
                if (item.X > max.X) max.X = item.X;

                if (item.Y < min.Y) min.Y = item.Y;
                if (item.Y > max.Y) max.Y = item.Y;
            }

            return new MyUnit(min.X+(max.X - min.X)/2, min.Y + (max.Y - min.Y) / 2);
        }
    }
}