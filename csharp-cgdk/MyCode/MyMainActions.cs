﻿using System;
using System.Linq;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    internal class MyMainActions
    {
        private MyStrategy ms;
        public double SqrTacticalNuclearStrikeRadius;
        private int _cooldown;
        private int _strikeTiker;
        private MyUnit _strikePoint;

        public MyMainActions(MyStrategy myStrategy)
        {
            this.ms = myStrategy;
            SqrTacticalNuclearStrikeRadius = ms.Game.TacticalNuclearStrikeRadius * ms.Game.TacticalNuclearStrikeRadius;
            NextTarget = new MyUnit(ms.Game.WorldWidth - ms.Game.VehicleRadius * 10, ms.Game.WorldHeight - ms.Game.VehicleRadius*10);
        }

        public MyUnit NextTarget { get; private set; }

        internal MyCommand Run()
        {
            Update();

            var myGroup = new MyGroup(ms.MyVehicles);
            var center = myGroup.Center;

            var pl = ms.World.GetOpponentPlayer();
            if (pl.NextNuclearStrikeVehicleId > 0)
            {
                if (_strikePoint == null)
                {
                    _strikePoint = new MyUnit(pl.NextNuclearStrikeX, pl.NextNuclearStrikeY);
                    if (myGroup.Units.Any(a => a.GetSquaredDistanceTo(_strikePoint) < SqrTacticalNuclearStrikeRadius))
                    {
                        NextTarget = center;
                        return new MyCommand.Scale(_strikePoint, 10);
                    }
                }
                else
                {
                    _strikeTiker++;
                    return null;
                }
            }

            if (_strikeTiker > 0)
            {
                if (_strikePoint != null)
                {
                    var strikePoint = _strikePoint;
                    _strikePoint = null;
                    NextTarget = center;
                    return new MyCommand.Scale(strikePoint, 0.1);
                }
                _strikeTiker--;
                return null;
            }

            myGroup.NextTarget = NextTarget;
            var enemy = ms.EnemyVehicles.OrderBy(v=>v.GetSquaredDistanceTo(center)).ToArray();

            if (enemy.Length < 1) return null;

            if (ms.Me.RemainingNuclearStrikeCooldownTicks == 0)
            {
                if (enemy[0].GetDistanceTo(center) < ms.Game.FighterVisionRange)
                {
                    var tacticalNuclearStrikeDelay = ms.Game.TacticalNuclearStrikeDelay - ms.World.GetOpponentPlayer().RemainingNuclearStrikeCooldownTicks;
                    if (tacticalNuclearStrikeDelay < 0)
                        tacticalNuclearStrikeDelay = 0;

                    var ally = myGroup.Units
                        .OrderBy(v => v.GetSquaredDistanceTo(center))
                        .OrderByDescending(v => v.CurVisionRange)
                        .ToArray();

                    var nucTargets = enemy
                        .Where(t => t.GetDistanceTo(center) < ms.Game.FighterVisionRange * 4).ToArray();

                    var nucTargetsStats1 = nucTargets
                        .Where(t => t.GetDistanceTo(center) < ms.Game.FighterVisionRange * 4)
                        .Select(t =>
                            new
                            {
                                point = t,
                                ghosts = ally.Where(a =>
                                        t.GetSquaredDistanceTo(a) < (a.CurVisionRange - a.MaxSpeed * tacticalNuclearStrikeDelay)*(a.CurVisionRange - a.MaxSpeed * tacticalNuclearStrikeDelay)
                                        && t.GetSquaredDistanceTo(a.FeaturePoint(ms.Game.TacticalNuclearStrikeDelay)) < a.SquaredCurVisionRange //TODO добавить FeaturePoint
                                    ).ToArray()
                            })
                        .Where(n => n.ghosts.Length > 0);

                    var nucTargetsStats2 = nucTargetsStats1
                        .Select(t =>
                            new
                            {
                                t.point,
                                t.ghosts,
                                enemys = nucTargets.Select(a => new NuckTarget(a, t.point)).Where(a => a.Damage > 0).ToArray(),
                                allyDamage = ally.Sum(a => {
                                    var dist = t.point.GetSquaredDistanceTo(a.FeaturePoint(ms.Game.TacticalNuclearStrikeDelay));
                                    if (dist > SqrTacticalNuclearStrikeRadius)
                                    {
                                        return 0d;
                                    }
                                    dist = Math.Sqrt(dist);
                                    return ms.Game.MaxTacticalNuclearStrikeDamage * (1 - dist / MyCommand.ms.Game.TacticalNuclearStrikeRadius);
                                })
                            });
                        //.Where(n => n.enemys.Length > 1);

                    var nucTargetsStats = nucTargetsStats2
                        .Select(t =>
                            new
                            {
                                t.point,
                                t.ghosts,
                                t.enemys,
                                killed = t.enemys.Count(e=>e.IsDead),
                                damage = t.enemys.Sum(e => e.Damage),
                                t.allyDamage
                            })
                        .Where(n => n.damage > n.allyDamage)
                        .OrderByDescending(n => n.damage)
                        .OrderBy(n => n.allyDamage)
                        .OrderByDescending(n => n.killed)
                        .ToArray();

                    if (nucTargetsStats.Length > 0)
                    {
                        var target = nucTargetsStats[0];
                        return new MyCommand.NuclearStrike(target.ghosts.OrderBy(a => a.GetSquaredDistanceTo(center)).First(), target.point);
                    }
                }
            }


            //враг далеко
            if (enemy[0].GetDistanceTo(center) > ms.Game.FighterVisionRange)
            {
                //еще не идем к нему
                if (NextTarget.GetDistanceTo(enemy[0]) > ms.Game.FighterVisionRange)
                {
                    _cooldown = 0;
                    NextTarget = new MyUnit(enemy[0]);
                    return new MyCommand.MoveTo(center, enemy[0], myGroup.Units.Min(u=>u.MinSpeed));
                }
            }
            //враг рядом
            else
            {
                //занять позицию
                if (_cooldown < 1)
                {
                    var ally = myGroup.Units
                        .Where(a=>a.CanAttack(enemy[0]))
                        .OrderBy(v => v.GetSquaredDistanceTo(enemy[0]))
                        .ToArray();

                    if (ally.Length > 0)
                    {
                        var attaker = center;
                        var enemyDist = attaker.GetDistanceTo(enemy[0]);
//                        var attakPos = enemyDist - (!enemy[0].IsAerial ? attaker.GroundAttackRange : attaker.AerialAttackRange) + 1;
                        var attakPos = enemyDist - 5;

                        if (attakPos > 0.5)
                        {
                            var x = enemy[0].X - attaker.X;
                            var y = enemy[0].Y - attaker.Y;

                            var sinX = x / enemyDist;
                            var sinY = y / enemyDist;

                            x = attakPos * sinX;
                            y = attakPos * sinY;

                            var minSpeed = myGroup.Units.Min(u => u.MinSpeed);

                            _cooldown = (int)Math.Ceiling(attakPos / minSpeed);

                            if (_cooldown < 1) _cooldown = 20;
                            if (_cooldown > 60) _cooldown = 60;

                            NextTarget = new MyUnit(x, y);
                            return new MyCommand.MoveTo(x, y, minSpeed);
                        }
                        else
                        {
                            _cooldown = 20;
                            NextTarget = center;
                            return new MyCommand.Scale(attaker, 0.1);
                        }
                    }
                }
            }

            return null;
        }

        private void Update()
        {
            _cooldown--;
        }

        private class NuckTarget
        {
            private MyVehicle veickle;
            private MyUnit point;

            public NuckTarget(MyVehicle vehicle, MyUnit point)
            {
                this.veickle = vehicle;
                this.point = point;
                var dist = vehicle.GetSquaredDistanceTo(point);

                if (dist < MyConsts.SqrTacticalNuclearStrikeRadius)
                {
                    dist = Math.Sqrt(dist) + vehicle.MaxSpeed * MyCommand.ms.Game.TacticalNuclearStrikeDelay;
                    if (dist < MyCommand.ms.Game.TacticalNuclearStrikeRadius)
                    {
                        Damage = MyCommand.ms.Game.MaxTacticalNuclearStrikeDamage * (1 - dist / MyCommand.ms.Game.TacticalNuclearStrikeRadius);
                    }
                }
            }

            public double Damage { get; }
            public bool IsDead => Damage >= veickle.Durability;
        }
    }
}