﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Diagnostics;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    [DebuggerDisplay("{Type} [{Units.Length}] ({Center})")]
    public class MyGroup : IMyRectangle
    {
        private MyUnit _center;
        private MyUnit _min;
        private MyUnit _max;
        private MyUnit _nextTarget;

        public MyUnit Center => _center ?? Lazy(() => _center);
        public MyUnit Min => _min ?? Lazy(() => _min);
        public MyUnit Max => _max ?? Lazy(() => _max);

        private MyUnit Lazy(Func<MyUnit> get)
        {
            Update();
            return get();
        }

        private void Update()
        {
            var max = new MyUnit();
            var min = new MyUnit(double.MaxValue, double.MaxValue);
            foreach (var item in Units)
            {
                if (item.X < min.X) min.X = item.X;
                if (item.X > max.X) max.X = item.X;

                if (item.Y < min.Y) min.Y = item.Y;
                if (item.Y > max.Y) max.Y = item.Y;
            }

            _center = new MyUnit(min.X + (max.X - min.X) / 2, min.Y + (max.Y - min.Y) / 2);
            _min = min;
            _max = max;
        }

        public MyVehicle[] Units { get; }
        public double MaxSpeed => Units[0].MaxSpeed;
        public VehicleType Type => Units[0].Type;

        public MyUnit Target { get; internal set; }
        public MyUnit NextTarget {
            get { return _nextTarget; }
            internal set
            {
                if (Equals(_nextTarget, value)) return;
                _nextTarget = value;

                if (_nextTarget == null)
                {
                    CurPathTarget = null;
                    return;
                }

                var movX = _nextTarget.X - Center.X;
                var movY = _nextTarget.Y - Center.Y;

                CurPathTarget = new MyRectangle(Min.X + movX, Min.Y + movY, Max.X + movX, Max.Y + movY);
            }
        }

        public bool IsEndMoving => Units.All(u => u.IsEndMoving);
        public bool IsStoped => Units.All(u => u.IsStoped);

        public MyGroup(IEnumerable<MyVehicle> enumerable)
        {
            this.Units = enumerable.ToArray();
        }

        internal void UpdateStats()
        {
            _center = null;
            _min = null;
            _max = null;
        }

        internal MyRectangle CurPath {
            get
            {
                if (CurPathTarget == null) return new MyRectangle(Min, Max);
                return CurPathTarget.From(this);
            }
        }

        internal MyRectangle CurPathTarget { get; private set; }
        public bool IsAerial => Units[0].IsAerial;

        internal MyRectangle Path(MyUnit nextTarget)
        {
            var movX = nextTarget.X - Center.X;
            var movY = nextTarget.Y - Center.Y;

            var xs = new[]
            {
                Min.X,
                Max.X,
                Min.X+movX,
                Max.X+movX,
            };
            var ys = new[]
            {
                Min.Y,
                Max.Y,
                Min.Y+movY,
                Max.Y+movY,
            };

            var pMin = new MyUnit(xs.Min(), ys.Min());
            var pMax = new MyUnit(xs.Max(), ys.Max());

            return new MyRectangle(pMin, pMax);
        }

        //public override string ToString()
        //{
        //    return $"{Type} [{Units.Length}] ({Center})";
        //}
    }
}