﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public class MyButerBuilder
    {
        public MyGroup Arrvs { get; }
        public MyGroup Ifvs { get; }
        public MyGroup[] Ground { get; }
        public MyGroup[] Air { get; }
        public MyGroup[] All { get; }
        public MyGroup Tanks { get; }

        private MyUnit[] _start;
        private readonly MyStrategy _myStrategy;
        private int _step;

        public bool IsComplet { get; private set; }
        public int Tick => _myStrategy.World.TickIndex;

        private Stack<MyCommand> Moves = new Stack<MyCommand>();

        public MyButerBuilder(MyStrategy ms)
        {
            var tanks = new MyGroup(ms.MyVehicles.Where(x => x.Type == VehicleType.Tank));
            var arrvs = new MyGroup(ms.MyVehicles.Where(x => x.Type == VehicleType.Arrv));
            var ifvs = new MyGroup(ms.MyVehicles.Where(x => x.Type == VehicleType.Ifv));

            var fighters = new MyGroup(ms.MyVehicles.Where(x => x.IsMy && x.Type == VehicleType.Fighter));
            var helicopters = new MyGroup(ms.MyVehicles.Where(x => x.IsMy && x.Type == VehicleType.Helicopter));

            this.Tanks = tanks;
            this.Arrvs = arrvs;
            this.Ifvs = ifvs;
            this._myStrategy = ms;
            Ground = new[] { tanks, arrvs, ifvs };
            Air = new[] { fighters, helicopters };
            All = Ground.Concat(Air).ToArray();

            _start = new[] {
                new MyUnit(45, 119),
                new MyUnit(119, 119),
                new MyUnit(193, 119)
            };
        }

        internal bool SelectStart(MyGroup[] groups)
        {
            var formove = groups.Where(u => _start.All(s => u.Center.GetSquaredDistanceTo(s) > 2) && u.Target == null).OrderBy(u=>u.MaxSpeed).ToArray();
            if (formove.Length < 1) return false;

            var forFell = _start.Where(s => groups.All(u => u.Center.GetSquaredDistanceTo(s) > 2)).ToList();
            while (forFell.Count >  _start.Length - groups.Length)
            {
                var en = formove.Where(fm => fm.Target == null).GetEnumerator();
                var next = en.MoveNext();
                while (next)
                {
                    var u = en.Current;
                    var dist = forFell.Min(s => u.Center.GetSquaredDistanceTo(s))+2;
                    var targets = forFell.Where(s => u.Center.GetSquaredDistanceTo(s) < dist).OrderBy(s => u.Center.GetSquaredDistanceTo(s)).ToArray();
                    next = en.MoveNext();
                    if (targets.Length > 1 && next) continue;
                    u.Target = targets[0];
                    forFell.Remove(targets[0]);
                }
            }

            return true;
        }

        public MyCommand Build()
        {
            foreach (var item in All)
            {
                item.UpdateStats();
            }

            if (_step == 0)
            {
                SelectStart(Ground);
                SelectStart(Air);

                //сортируем группы по позициям
                var all = Ground.ToArray();
                for (int i = 0; i < 3; i++)
                {
                    Ground[i] = all.First(u => u.Center.GetSquaredDistanceTo(_start[i]) < 2 || (u.Target != null && u.Target.Equal(_start[i])));
                }

                _step = 1;
            }

            if (_step == 1)
            {
                foreach (var item in All.Where(u => u.Target != null).OrderBy(t => t.MaxSpeed))
                {
                    //приехали
                    if (item.Center.GetSquaredDistanceTo(item.Target) < 0.001)
                    {
                        item.Target = null;
                        continue;
                    }

                    if (item.NextTarget == null || item.IsEndMoving)
                    {
                        item.NextTarget = null;
                        var center = item.Center;
                        var target = item.Target;
                        //по Y
                        if (center.X.Equal(target.X) || !center.Y.Equal(target.Y))
                        {
                            var nextTarget = new MyUnit(center.X, target.Y);
                            if (All.Where(u=>u.IsAerial == item.IsAerial).All(
                                u => u == item
                                || !item.Path(nextTarget).IsIntersect(u.CurPath)
                                ))
                            {
                                item.NextTarget = nextTarget;
                                return new MyCommand.MoveGroup(item, item.NextTarget);
                            }
                        }
                        //по X
                        if (center.Y.Equal(target.Y) || !center.X.Equal(target.X))
                        {
                            var nextTarget = new MyUnit(target.X, center.Y);
                            if (All.Where(u => u.IsAerial == item.IsAerial).All(u => u == item || !item.Path(nextTarget).IsIntersect(u.CurPath)))
                            {
                                item.NextTarget = nextTarget;
                                return new MyCommand.MoveGroup(item, item.NextTarget);
                            }
                        }
                    }
                }


                if (All.All(u => u.Target == null && u.IsEndMoving))
                {
                    _step = 2;

                    var step = 4;

                    var minX = Ground.Min(a => a.Min.X);
                    var maxX = Ground.Max(a => a.Max.X);

                    var y = Ground[0].Center.Y;

                    for (int i = 0; i < 5; i++)
                    {
                        var selectY = i * 6;
                        var moveY = (i + 1) * step + i * step;

                        Slice(minX, y + selectY, maxX, y + selectY + 6, 0, moveY);
                        Slice(minX, y - selectY - 6, maxX, y - selectY, 0, -moveY);
                    }
                }
            }

            if (_step == 2)
            {
                if (Moves.Count > 0)
                    return Moves.Pop();

                if (All.All(u => u.IsEndMoving|| u.IsStoped))
                {
                    Moves.Push(new MyCommand.MoveRectangle(Ground[0], 0, -0.5-4));
                    Moves.Push(new MyCommand.MoveRectangle(Ground[2], 0, 0.5 +4));

                    _step = 3;
                }
            }

            if (_step == 3)
            {
                if (Moves.Count > 0)
                    return Moves.Pop();

                if (All.All(u => u.IsEndMoving|| u.IsStoped))
                {
                    var x = Ground[1].Center.X - Ground[0].Center.X;

                    Moves.Push(new MyCommand.MoveRectangle(Ground[0], x, 0));
                    Moves.Push(new MyCommand.MoveRectangle(Ground[2], -x, 0));

                    _step = 4;
                }
            }

            if (_step == 4)
            {
                if (Moves.Count > 0)
                    return Moves.Pop();

                if (All.All(u => u.IsEndMoving || u.IsStoped))
                {
                    _step = 5;

                    return new MyCommand.SelectRectangle(new MyRectangle(Ground[0].Min, Ground[2].Max));
                }
            }

            if (_step == 5)
            {
                _step = 6;

                return new MyCommand.Rotate(Ground[1].Center, Math.PI / 4);
            }

            if (_step == 6)
            {
                if (!All.All(u => u.IsStoped)) return null;

                _step = 7;

                return new MyCommand.Scale(Ground[1].Center, 0.1);
            }

            if (_step == 7)
            {
                if (!All.All(u => u.IsStoped)) return null;

                IsComplet = true;

                return new MyCommand.MoveTo(_myStrategy.Game.WorldWidth - 200, _myStrategy.Game.WorldHeight - 200, Tanks.MaxSpeed * _myStrategy.Game.SwampTerrainSpeedFactor);
            }

            return null;
        }

        private void Slice(double minX, double minY, double maxX, double maxY, int moveX, int moveY)
        {
            Moves.Push(new MyCommand.MoveRectangle(minX, minY, maxX, maxY, moveX, moveY));
        }
    }

    public enum MyDir
    {
        None,
        X,
        Y
    }
}