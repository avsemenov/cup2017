﻿using System;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Linq;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    public abstract class MyCommand
    {
        public static MyStrategy ms;

        public class MoveGroup : MyCommand
        {
            private MyGroup item;
            private double moveX;
            private double moveY;

            public MoveGroup(MyGroup item, MyUnit nextTarget, MyDir block = MyDir.None)
            {
                this.item = item;
                if (block != MyDir.X)
                    moveX = nextTarget.X - item.Center.X;
                if (block != MyDir.Y)
                    moveY = nextTarget.Y - item.Center.Y;
            }

            public MoveGroup(MyGroup item, double x, double y)
            {
                this.item = item;
                moveX = x;
                moveY = y;
            }

            public override bool Run(Move move)
            {
                if (!item.Units.All(u => u.IsSelected))
                {
                    Select(move, item);
                    return false;
                }

                Mov(move, moveX, moveY);
                return true;
            }

        }

        private void Select(Move move, MyGroup item)
        {
            Select(move, item.Min.X, item.Min.Y, item.Max.X, item.Max.Y, item.Type);
//            Select(move, item.Min.X, item.Min.Y, item.Max.X, item.Max.Y);
        }

        private void Select(Move move, MyRectangle item)
        {
            Select(move, item.Min.X, item.Min.Y, item.Max.X, item.Max.Y);
        }

        public abstract bool Run(Move move);

        private void SaveSelect(Move move, int v)
        {
            move.Action = ActionType.Assign;
            move.Group = v;
        }

        private void LoadSelect(Move move, int v)
        {
            move.Action = ActionType.ClearAndSelect;
            move.Group = v;
        }

        private void Mov(Move move, double x, double y, double maxSpeed = 0)
        {
            var c = Math.Sqrt(x * x + y * y);
            var sinX = c == 0 ? 0 : x / c;
            var sinY = c == 0 ? 0 : y / c;

            foreach (var unit in ms.MyVehicles.Where(v=>v.IsSelected))
            {
                unit.NextTarget.X = unit.X + x;
                unit.NextTarget.Y = unit.Y + y;
                unit.Sin.X = sinX;
                unit.Sin.Y = sinY;
                unit.Speed = maxSpeed;
            }

            move.Action = ActionType.Move;
            move.X = x;
            move.Y = y;
            move.MaxSpeed = maxSpeed;
        }

        private void Select(Move move, double minX, double minY, double maxX, double maxY)
        {
            move.Action = ActionType.ClearAndSelect;
            move.Left = minX - 1;
            move.Top = minY - 1;
            move.Right = maxX + 1;
            move.Bottom = maxY + 1;
        }

        private void Select(Move move, double minX, double minY, double maxX, double maxY, VehicleType vehicleType)
        {
            Select(move, minX, minY, maxX, maxY);
            move.VehicleType = vehicleType;
        }

        internal class MoveRectangle : MyCommand
        {
            private double minX;
            private double minY;
            private double maxX;
            private double maxY;
            private double x;
            private double y;
            private bool _isSelected;

            public MoveRectangle(IMyRectangle rectangle, double x, double y) : this(rectangle.Min.X, rectangle.Min.Y, rectangle.Max.X, rectangle.Max.Y, x, y)
            {
            }

            public MoveRectangle(double minX, double minY, double maxX, double maxY, double x, double y)
            {
                this.minX = minX;
                this.minY = minY;
                this.maxX = maxX;
                this.maxY = maxY;
                this.x = x;
                this.y = y;
            }

            public override bool Run(Move move)
            {
                if (!_isSelected)
                {
                    _isSelected = true;
                    Select(move, minX,minY,maxX,maxY);
                    return false;
                }

                var rect = new MyRectangle(minX - 1, minY - 1, maxX + 1, maxY + 1);

                Mov(move, x, y);
                return true;
            }
        }

        internal class SelectRectangle : MyCommand
        {
            private MyRectangle myRectangle;

            public SelectRectangle(MyRectangle myRectangle)
            {
                this.myRectangle = myRectangle;
            }

            public override bool Run(Move move)
            {
                Select(move, myRectangle);
                return true;
            }
        }

        internal class Scale : MyCommand
        {
            private readonly MyUnit center;
            private double factor;

            public Scale(MyUnit center, double factor)
            {
                this.center = center;
                this.factor = factor;
            }

            public override bool Run(Move move)
            {
                move.Action = ActionType.Scale;
                move.Factor = factor;
                move.X = center.X;
                move.Y = center.Y;
                return true;
            }
        }

        internal class Rotate : MyCommand
        {
            private MyUnit center;
            private double v;

            public Rotate(MyUnit center, double v)
            {
                this.center = center;
                this.v = v;
            }

            public override bool Run(Move move)
            {
                move.Action = ActionType.Rotate;
                move.Angle = v;
                move.X = center.X;
                move.Y = center.Y;
                return true;
            }
        }

        internal class MoveTo : MyCommand
        {
            private double x;
            private double y;

            public MoveTo(double x, double y, double maxSpeed)
            {
                this.x = x;
                this.y = y;
                this.maxSpeed = maxSpeed;
            }

            public MoveTo(MyUnit from, double x, double y, double maxSpeed)
            {
                this.x = x - from.X;
                this.y = y - from.Y;
                this.maxSpeed = maxSpeed;
            }

            public MoveTo(MyUnit from, MyUnit to, double maxSpeed)
            {
                this.x = to.X - from.X;
                this.y = to.Y - from.Y;
                this.maxSpeed = maxSpeed;
            }

            private double maxSpeed;

            public override bool Run(Move move)
            {
                Mov(move, x, y, maxSpeed);
                return true;
            }
        }

        internal class NuclearStrike : MyCommand
        {
            private readonly MyVehicle ghost;
            private readonly MyUnit center;

            public NuclearStrike(MyVehicle ghost, MyUnit center)
            {
                this.ghost = ghost;
                this.center = center;
            }

            public override bool Run(Move move)
            {
                move.Action = ActionType.TacticalNuclearStrike;
                move.VehicleId = ghost.Id;
                move.X = center.X;
                move.Y = center.Y;
                return true;
            }
        }
    }
}