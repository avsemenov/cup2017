﻿using System;
using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System.Diagnostics;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    [DebuggerDisplay("{Type} ({X}, {Y})")]
    public class MyVehicle : MyUnit
    {
        public World World => MyCommand.ms.World;
        public Game Game => MyCommand.ms.Game;
        private MyStrategy ms => MyCommand.ms;

        private Vehicle V;
        private bool _isStoped;

        public bool IsAerial => V.IsAerial;

        public long Id => V.Id;
        public VehicleType Type => V.Type;
        public bool IsMy => V.PlayerId == ms.Me.Id;

        public int Durability { get; private set; }
        public bool IsSelected { get; private set; }
        public int RemainingAttackCooldownTicks { get; private set; }
        public MyUnit NextTarget { get; internal set; }
        public bool IsEndMoving => Equal(NextTarget);
        public bool IsStoped => _isStoped || IsDead || UpdateTick < World.TickIndex;
        public bool IsDead { get; set; }

        public double MaxSpeed => V.MaxSpeed;
        public double MinSpeed => V.MaxSpeed * (IsAerial ? 1: Game.SwampTerrainSpeedFactor);

        public int UpdateTick { get; private set; }
        public double SquaredCurVisionRange => CurVisionRange * CurVisionRange;

        public double GroundAttackRange => V.GroundAttackRange;
        public double AerialAttackRange => V.AerialAttackRange;

        public double CurVisionRange { get; internal set; }
        public MyUnit Sin { get; internal set; }
        public double Speed { get; internal set; }

        public MyVehicle(Vehicle v)
        {
            V = v;
            X = v.X;
            Y = v.Y;
            CurVisionRange = v.VisionRange * Factor(X, Y);

            NextTarget = new MyUnit(this);
            Sin = new MyUnit();
        }

        internal void Update(VehicleUpdate v, int tickIndex)
        {
            UpdateTick = tickIndex;

            _isStoped = (X == v.X && Y == v.Y);
            if (!_isStoped)
            {
                X = v.X;
                Y = v.Y;
                CurVisionRange = V.VisionRange * Factor(X,Y);
            }

            Durability = v.Durability;
            if (v.Durability == 0)
            {
                IsDead = true;
            }
            IsSelected = v.IsSelected;
            RemainingAttackCooldownTicks = v.RemainingAttackCooldownTicks;
        }

        public MyUnit FeaturePoint(int ticks)
        {
            if (IsStoped || IsEndMoving) return this;
            var c = (Speed > 0 ? Speed : V.MaxSpeed) * ticks;
            return new MyUnit(c * Sin.X + X, c * Sin.Y + Y);
        }

        private double Factor(double x, double y)
        {
            switch (ms.WeatherByCellXY[(int)(x / 32)][(int)(y / 32)])
            {
                case WeatherType.Clear:
                    return ms.Game.ClearWeatherVisionFactor;
                case WeatherType.Cloud:
                    return ms.Game.CloudWeatherVisionFactor;
                case WeatherType.Rain:
                    return ms.Game.RainWeatherVisionFactor;
            }
            return 1;
        }

        internal bool CanAttack(MyVehicle e)
        {
            switch (Type)
            {
                case VehicleType.Arrv:
                    return false;
                case VehicleType.Fighter:
                    return e.IsAerial;
                case VehicleType.Helicopter:
                    return true;
                case VehicleType.Ifv:
                    return true;
                case VehicleType.Tank:
                    return e.Type != VehicleType.Fighter;
            }
            return false;
        }
    }
}