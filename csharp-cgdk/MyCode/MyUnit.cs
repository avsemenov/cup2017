﻿using Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk.Model;
using System;
using System.Diagnostics;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    [DebuggerDisplay("{X}, {Y}")]
    public class MyUnit
    {
        public double X;
        public double Y;

        public MyUnit()
        { }

        public MyUnit(double x, double y)
        {
            X = x;
            Y = y;
        }

        public MyUnit(MyUnit u)
        {
            X = u.X;
            Y = u.Y;
        }

        public double GetDistanceTo(double x, double y)
        {
            double xRange = x - this.X;
            double yRange = y - this.Y;
            return Math.Sqrt(xRange * xRange + yRange * yRange);
        }

        public double GetDistanceTo(MyUnit unit)
        {
            return GetDistanceTo(unit.X, unit.Y);
        }

        public double GetDistanceTo(Unit unit)
        {
            return GetDistanceTo(unit.X, unit.Y);
        }

        public double GetSquaredDistanceTo(double x, double y)
        {
            double xRange = x - this.X;
            double yRange = y - this.Y;
            return xRange * xRange + yRange * yRange;
        }

        public double GetSquaredDistanceTo(Unit unit)
        {
            return GetSquaredDistanceTo(unit.X, unit.Y);
        }

        public double GetSquaredDistanceTo(MyUnit unit)
        {
            return GetSquaredDistanceTo(unit.X, unit.Y);
        }

        public bool Equal(MyUnit nextTarget)
        {
            return nextTarget != null && X.Equal(nextTarget.X) && Y.Equal(nextTarget.Y);
        }

        public bool Equal(MyUnit nextTarget, double epsilon)
        {
            return nextTarget != null && X.Equal(nextTarget.X, epsilon) && Y.Equal(nextTarget.Y, epsilon);
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }
    }
}