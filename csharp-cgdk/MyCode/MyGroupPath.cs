﻿using System;

namespace Com.CodeGame.CodeWars2017.DevKit.CSharpCgdk
{
    internal class MyRectangle : IMyRectangle
    {
        public MyRectangle(MyUnit pMin, MyUnit pMax)
        {
            this.Min = pMin;
            this.Max = pMax;
        }

        public MyRectangle(double minX, double minY, double maxX, double maxY)
        {
            this.Min = new MyUnit(minX, minY);
            this.Max = new MyUnit(maxX, maxY);
        }

        public MyUnit Min { get; set; }
        public MyUnit Max { get; set; }

        internal bool IsIntersect(MyRectangle r2)
        {
            return !( r2.Min.X > Max.X ||
               r2.Max.X < Min.X ||
               r2.Max.Y < Min.Y ||
               r2.Min.Y > Max.Y );
        }

        internal MyRectangle From(MyGroup myGroup)
        {
            return new MyRectangle(GetMin(Min.X,myGroup.Min.X), GetMin(Min.Y, myGroup.Min.Y), GetMax(Max.X, myGroup.Max.X), GetMax(Max.Y, myGroup.Max.Y));
        }

        private double GetMax(double d1, double d2)
        {
            return d1 > d2 ? d1 : d2;
        }

        private double GetMin(double d1, double d2)
        {
            return d1 < d2 ? d1 : d2;
        }

        internal bool IsInclude(MyUnit v)
        {
            return Max.X > v.X && v.X > Min.X  && Max.Y > v.Y && v.Y > Min.Y;
        }
    }

    internal interface IMyRectangle
    {
        MyUnit Min { get; }
        MyUnit Max { get; }
    }
}